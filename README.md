# Lissajous Patterns - Multisim

Free simulation files for creating Lissajous patterns also know as Bowditch 
curves. Some files will have full circuits while others may only have AC voltage
supplies to manipulate frequency and phase shift for theory.

[](www.bits4bots.com)
[](www.https://bits4bots.co)